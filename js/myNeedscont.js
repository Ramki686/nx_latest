/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.controller('myNeedscont', function($scope,$location, $localStorage){
    
     $scope.withoutLogin = true;
        $scope.H_ome = false;
        $scope.m_ain = false;
    $scope.ndFunction = function()
    {
        var userEmail = $localStorage.Email;
         //   alert('$scope.showhide' + userEmail);
            if(userEmail == undefined)
            {
                $scope.withoutLogin = true;
                 $scope.H_ome = true;
                  $scope.m_ain = false;
            }else{
                 $scope.withoutLogin = false;
                  $scope.m_ain = true;
                  $scope.H_ome = false;
                  $scope.withLogin = true;
            }
        };
        
         
        $scope.Logout = function()
        {
              $localStorage.$reset();
              localStorage.clear();
             $location.path('/Home');

        };

       /*to send user back to registration page based on his role he chose*/

       $scope.checkroleofuser = function()
       {

        //alert($localStorage.person_value);
        if($localStorage.person_value == 'NGO'){
          $location.path('/Ngo_reg');
        }
         else if ($localStorage.person_value == 'Donor_Volunteer')
         {
          $location.path('/Donor_Volunteer');
         }
         else if($localStorage.person_value == undefined){
          alert('Please register or login to continue using the application');
          $location.path('/Home');
         }
       }
        
         $scope.getall_details = function()
                    {
                      $scope.F_Name = $localStorage.FirstName;
                      $scope.L_Name = $localStorage.LastName;
                      $scope.User_name = $localStorage.displayName;
                      $scope.genDer = $localStorage.gender;
                      $scope.D_OO_B = $localStorage.DOO_B;
                      $scope.Ema_il = $localStorage.Email;
                      $scope.Image = $localStorage.Image;
                     
                    }
                    $scope.isActive = function (viewLocation) {
             var active = (viewLocation === $location.path());
            return active;
};
    
});
