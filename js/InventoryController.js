
'use strict';

app.controller('InventoryController',['$scope','$http','$location','$localStorage',function($scope,$http,$location,$localStorage,$filter) {


var person = getUrl("/api/person");
var requests = getUrl("/api/requests");/*working api*/
var personDonations = getUrl("/api/personDonations");/*working api*/
var locations = getUrl("/api/locations");/*working api*/
var pickupRequest = getUrl("/api/pickupRequest");/*working api*/
var pickupRequestDetail = getUrl("/api/pickupRequestDetail");/*working api*/
var donations = getUrl("/api/donations");/*working api*/
var deliveryDetail = getUrl("/api/deliveryDetail");/*working api*/
var pickupRequestDetail = getUrl("/api/pickupRequestDetail");/*working api*/
var orgPersonRoleMap = getUrl("/api/orgPersonRoleMap");/*working api*/
var items = getUrl("/api/items");/*working api*/
var person = getUrl("/api/person");/*working api*/
var organization = getUrl("/api/organization");/*working api*/
var categories = getUrl("/api/categories");/*working api*/
var subCategories = getUrl("/api/subCategories");/*working api*/



  

  $scope.AddInventory = function () {

    
    $localStorage.option1 = $scope.option1;
    $localStorage.option2 = $scope.option2;
    $localStorage.quantity = $scope.mySwitch;
    $scope.viewINventory = true;
    $scope.INEN_te = true;
    
  };



  

  $scope.inven= function()
  {
    $localStorage.option1=$scope.option1;
    $localStorage.option2=$scope.option2;
    $localStorage.quantity=$scope.mySwitch;

  };

  $scope.changeInventory=function(){

    $localStorage.item1=$scope.option1;
    $localStorage.item2=$scope.option2;
    $localStorage.quant=$scope.mySwitch;
  
  };
  

  /*Getting Dropdown value of the selected Inventory*/
   $scope.Inventory = function (p)
   {

   localStorage.setItem('Inventory',p);
  /* $scope.getINVEN = localStorage.getItem('Inventory');
   //console.log($scope.getINVEN);*/
   location.reload();

   };

   /*Displaying data based on selection of the Inventory Dropdown (Show and Hide)*/
  $scope.INEN_te = false;
  $scope.viewINventory = false;

  $scope.sho_Add_ViewInV = function ()
  {
  $scope.getINVEN = localStorage.getItem('Inventory');
  //console.log('Inventory in Details:  '+$scope.getINVEN);
  if($scope.getINVEN == 'AddInv')
  {
    $scope.INEN_te = true;
  }
  else if($scope.getINVEN == 'ViewInv')
  {
    $scope.viewINventory = true;
  }
  };


  
       

  
  
  /*Get Person ID*/
    

          /*Get the orgNames*/

                    $scope.getOrgName = function()
                    {
                   $http({
                              url: organization,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                        }).then(function(response)
                              {
                                  //console.log(response);
                                  $scope.Org_name = response.data;
                                  $scope.IN_org_Name = [];
                                  $scope.IN_org_Name_ID = [];
                                  for(var i=0; i< $scope.Org_name.data.length; i++)
                                  {
                                      $scope.IN_org_Name.push($scope.Org_name.data[i].orgName);
                                      $scope.IN_org_Name_ID.push($scope.Org_name.data[i].id);
                                      
                                  }
                                  //console.log($scope.Org_name);
                                  //console.log($scope.IN_org_Name);
                                   $scope.orgrolemap();
                                  
                                  
                                     
                              },
                              function(response) 
                              {
                              alert('Error');

                              });  
                };


                           /*get items*/

                            $scope.getItems=function(){
                                
                                $http({
                              url: items,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                             }).then(function(response)
                              {
                                  //console.log(response);
                                  $scope.itemName= response.data;
                                  $scope.it_ems=[];
                                  $scope.it_ems_ID=[];
                                  for(var i=0; i< $scope.itemName.data.length; i++)
                                  {
                                      $scope.it_ems.push($scope.itemName.data[i].itemName);
                                      $scope.it_ems_ID.push($scope.itemName.data[i].id);
                                      
                                  }
                                  //console.log($scope.itemName);
                                  //console.log($scope.it_ems);
                                  //console.log($scope.it_ems_ID);
                                //  $scope.it_ems__ID = $scope.it_ems_ID[$scope.it_ems.indexOf($scope.Item_Name)];

                                  
                                  },
                              function(response) 
                              {
                              alert('Error');

                              });                
                            }
                            $scope.get_orgID = function()
                            {
                                 $scope.it_ORG__ID = $scope.IN_org_Name_ID[$scope.IN_org_Name.indexOf($scope.org_name)];
                                   //alert($scope.it_ORG__ID);
                            };
                             $scope.get_itemID = function()
                            {
                                 $scope.it_ems__ID = $scope.it_ems_ID[$scope.it_ems.indexOf($scope.Item_Name)];
                                   //alert($scope.it_ems__ID);
                            };
                            $scope.get_person_ID = function()
                            {
                                 $scope.it_person__ID = $scope.person_ID[$scope.Person.indexOf($scope.personName)];
                                   //alert($scope.it_person__ID);
                            };
           
                            
                            /*Get the request details*/
                            
    
        
        /*Getting volunteer and Donor details*/
             $scope.get_person_role = function()
                    {
                         $http({
                        url: person,
                        method: "GET",
                        dataType: "json",

                        headers: {
                          'Content-Type' :'application/json'
                         },
                         }).then(function(response)
                          {

                            //console.log(response);
                            $scope.Person_data = response.data;
                            
                           // $scope.person_type = $scope.Person_data.data.personType;
                            //console.log($scope.Person_data);

                             $scope.person_Type = []; 
                             $scope.Volunteer_NAme = [];
                              $scope.Ngo_NAme= [];
                  //          $scope.Stae_id = []; 
                  //           $scope.Cites_id = []; 
                             //console.log($scope.Cites_id);F

                             for (var i=0;i<$scope.Person_data.data.length;i++)
                             {
                                   //$scope.ALL_staes = $scope.Citi_es.data[i];
                                     $scope.person_Type.push($scope.Person_data.data[i].personTypeId);
                                     $scope.pers_type =  $scope.person_Type[i];
                                     if($scope.pers_type == '3')
                                     {
                                         $scope.Volunteer_NAme.push($scope.Person_data.data[i].fullName);
                                     }
                                     if($scope.pers_type == '1')
                                     {
                                   $scope.Ngo_NAme.push($scope.Person_data.data[i].fullName);

                                     }
//                                     if(i == 3)
//                                      break;
                               }
         

                          },
                          function(response) {
                          alert('Errorrrrrrrrrrrrrr');

                          });   
                          

                    };
                    
                    
                    /*Harika*/
                    $scope.categeorys = function()
                    {
                   $http({
                              url: categories,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                           }).then(function(response)
                              {
                                 // //console.log(response);
                                  $scope.catogeory = response.data;
                                  
                                  $scope.name = [];
                                  $scope.categoryId = []; 
//                                  $scope.IN_org_Name_ID = [];
                                  for(var i=0; i< $scope.catogeory.data.length; i++)
                                  {
                                      $scope.name.push($scope.catogeory.data[i].categoryDesc);
                                      $scope.categoryId.push($scope.catogeory.data[i].id);
//                                      $scope.IN_org_Name_ID.push($scope.catogeory.data[i].id);
                                      
                                  }
                                  //console.log($scope.catogeory);
                                  //console.log($scope.name);
                                
                                     
                              },
                              function(response) 
                              {
                              alert('Error');

                              });  
                };
                 $scope.cate_id = function()
                
                    {
                        $scope.category_Id = $scope.categoryId[$scope.name.indexOf($scope.option1)];
                        
                        
                        $scope.sub_cat();
                    }
                     $scope.citieson_states = function()
                    {
                      //alert('dfgadf');
                          $scope.SubCategiries = [];
                           for(var i=0; i<$scope.Sub_ID.length; i++)
                           {            
                               if($scope.category_Id == $scope.cate_gory_Id[i])
                               {
                                 $scope.SubCategiries.push($scope.Sub_name[i]);                              
                               }             
                           }
                          // //console.log('ALL categories:   '+$scope.SubCategiries);        
                    };    

        $scope.sub_cat = function()
                    {
                         // alert('dfgadf');
                   $http({
                              url: subCategories,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                           }).then(function(response)
                              {
                                  //console.log(response);
                                  $scope.S_ubcatogeory = response.data;
                                  $scope.Sub_name = [];
                                  $scope.cate_gory_Id = []; 
                                  $scope.Sub_ID = []; 
                               
//                                  $scope.IN_org_Name_ID = [];
                                  for(var i=0; i< $scope.S_ubcatogeory.data.length; i++)
                                  {
                                      $scope.Sub_name.push($scope.S_ubcatogeory.data[i].subCategoryDesc);
                                      $scope.Sub_ID.push($scope.S_ubcatogeory.data[i].id);
                                      $scope.cate_gory_Id.push($scope.S_ubcatogeory.data[i].categoryId);
                                    
//                                      $scope.IN_org_Name_ID.push($scope.catogeory.data[i].id);
                                      
                                  }
                                  
                                  $scope.citieson_states();
                                 
//                                  alert($scope.Sub_name);
                                     
                              },
                              function(response) 
                              {
                              alert('Error');

                              });  
                };
               /*for getting the subcategories*/
                 $scope.subcat_id = function()
                            {
                                 $scope.categoryId = $scope.Sub_ID[$scope.Sub_name.indexOf($scope.option2)];
                             };


                 $scope.push_item = function()
                        {
                            $scope.pagination = true;
                            $scope.addrequestbtn = true;
                            $scope.category_Id ;
                            $scope.sub_categoryId = $scope.Sub_ID[$scope.Sub_name.indexOf($scope.option2)];
                            $scope.now=new Date();
                            $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                            var JSO_Ndata = {};
                            JSO_Ndata.id = '1';
                            JSO_Ndata.itemName = $scope.Item_Name;
                            JSO_Ndata.categoryId = $scope.category_Id;
                            JSO_Ndata.subCategoryId = $scope.sub_categoryId;
                            JSO_Ndata.units = $scope.totQuant;
                            JSO_Ndata.createdBy = '1';
                            JSO_Ndata.createdDatetime = $scope.Curre_date;
                            JSO_Ndata.lastUpdatedBy = '2';
                            JSO_Ndata.lastUpdatedDatetime = $scope.Curre_date;
                            
                      $http({
                      url: items,
                      method: "POST",
                      dataType: "json",

                      headers: {
                        'Content-Type' :'application/json'
                       },
                      data :JSO_Ndata
                         }).then(function(response) {
                        //console.log(response);
                        $scope.item_ID = response.data.data.id;
                         $scope.sendrequest($scope.item_ID);
                         
                      },
                      function(response) {
                        alert('Errorrrrrrrrrrrrrr');

                      });
                     
                       
                      
                        };
                        
                        /*Getting data from the orgPersonRoleMap table*/
                        
//                        $scope.getorgNames= function()
//                        {
//                            alert('getting the org names');
//                            
//                            
//                            
//                            
//                        }
                        
                        /*Sending Request to Request Table*/
                          $scope.NGO_table = true;
                         $scope.pagination = true;
                          $scope.addrequestbtn = true;
                        $scope.orgrolemap = function()
                        {
                        //    alert('dfgajdhjas');
                    $http({
                      url: orgPersonRoleMap,
                      method: "GET",
                      dataType: "json",

                      headers: {
                        'Content-Type' :'application/json'
                       }
                     
                         }).then(function(response) {
                        //alert(response);
                        //console.log(response);
                        $scope.orgRoledata = response.data.data;
                        $scope.pree_IID = $scope.orgRoledata[0].personId;
                        //console.log($scope.pree_IID);
                        //console.log('oRgaRole map');
                        //console.log($scope.orgRoledata);
                         $scope.Person_data = $localStorage.JsonOBJECT;
                         $scope.Per_ID = $scope.Person_data.id;
//                         alert('Local PersonID:  '+$scope.Per_ID);
                        for(var i =0; i < $scope.orgRoledata.length; i++)
                        {
                           // alert('helllo');
//                            alert($scope.orgRoledata[i].personId);
//                            alert($scope.Per_ID);
                            if($scope.Per_ID === $scope.orgRoledata[i].personId)
                            {
                                $localStorage.ORG_ID = $scope.orgRoledata[i].orgId;
//                                alert('inside orgRolemap');
                            }
                        }
                        
                      //$location.path('/AAAA');
                      },
                      function(response) {
                        alert('Errorrrrrrrrrrrrrr');

                      })
                      }
                        
            $scope.sendrequest=function(x){
            //console.log($scope.orgRoledata);
            $scope.Person_data = $localStorage.JsonOBJECT;
            $scope.ORGAA_id =  $localStorage.ORG_ID;
            $scope.Per_ID = $scope.Person_data.id;
                if( $scope.Rec_urring == true){
                    $scope.recurence= 'Y';
                    //console.log($scope.recurence);
                }
                else if ($scope.Rec_urring == false){
                    $scope.recurence = 'N';
                    //console.log($scope.recurence);
                }
//                for(var i=0; i<$scope.orgRoledata.length;i++)
//                {
//                    
//                }
                //alert($scope.freq);      
                $scope.neededBy= new Date($scope.needed_By);
                //alert( $scope.neededBy);
                $scope.requestDate= new Date();
                //alert($scope.requestDate);      
                $scope.now=new Date();      
                $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                  //alert($scope.Curre_date);
                    $http({
                      url: requests,
                      method: "POST",
                      dataType: "json",

                      headers: {
                        'Content-Type' :'application/json'
                       },
                      data :
                        {
                          'id':'1',
                          'orgId': $scope.ORGAA_id,
                          'itemId': x,
                          'personId':$scope.Per_ID,
                          'purpose':$scope.purpose,
                          'totalQuantityNeeded':$scope.totQuant,
                          'requestDate': $scope.requestDate,
                          'neededBy':$scope.neededBy,
                          'isRecurring':$scope.recurence,
                          'frequencyId':$scope.freq,
                          'createdBy':'1',
                          'createdDatetime':$scope.Curre_date,
                          'lastUpdatedBy':'2',
                          'lastUpdatedDatetime':$scope.Curre_date
                      }
                    }).then(function(response) {
                        //alert(response);
                        //console.log(response);
                        $scope.NGO_table = true;
                        $scope.ngoreq = false;
                        $scope.requestbtn = false;
                        $scope.addrequestbtn = true;
                        $scope.request_div = false;
                      //  $scope.sendpickupreq();
                        $scope.getallReqdetails();
                      //$location.path('/AAAA');
                      },
                      function(response) {
                        alert('Errorrrrrrrrrrrrrr');

                      })
                       document.pickup.reset();
                      $scope.pickup.$invalid=true;
                      $scope.addreq=true;
                      $scope.pickup.$invalid=false;
                     $scope.Item_Name=undefined;
                     $scope.category_Id=undefined;
                     $scope.sub_categoryId=undefined;
                     $scope.totQuant=undefined;
                     $scope.Curre_date=undefined;
                     $scope.purpose=undefined;

                  };
                  /*Send pckup requests from NGO*/
//                  $scope.sendpickupreq = function()
//                  {
//                      
//                  }
                 /*Display request div*/   
                 $scope.send_backvNgo = function()
                 {
                    $scope.ngoreq = false;
                    $scope.requestbtn = false;
                   
                     $scope.request_div = false;
                     $scope.NGO_table = true;
                      $scope.addrequestbtn = true;
                      $scope.pagination = true;


                      // $scope.pickup.$setPristine();
                      
                       $scope.Item_Name=undefined;
                     $scope.category_Id=undefined;
                     $scope.sub_categoryId=undefined;
                     $scope.totQuant=undefined;
                     $scope.Curre_date=undefined;
                     $scope.purpose=undefined;
                       
                      document.pickup.reset();
                     
                    
                 };
                 $scope.backtoagain = function()
                   {
                    //alert('helo');
                       $scope.request_div = false;
                          $scope.ngoreq = true;
                       $scope.requestbtn = true;

                    //  $scope.onacceptbtnclick = true; 
                  
                       }  
                 $scope.dis_requsetdiv = function()
                 {

                  $scope.ngoreq = false;
                  $scope.requestbtn = false;
                    $scope.addrequestbtn = false;
                     $scope.request_div = true;
                       $scope.NGO_table = false;
                       $scope.pagination = false;
                 };

                 $scope.NGO_table = true;
                  $scope.table_DIV = true;
                  $scope.addrequestbtn = true;
                  $scope.PAR_Ngo = false;
                  $scope.Req_Id;
                  $scope.ORG__ID;
                  $scope.person_IID;
                  $scope.PUrPOse;
                  $scope.NEDTime;
                  $scope.ItemNM;
                  $scope.requestsneedeby;
                  $scope.requesteddate;
                  $scope.currentquantity;
                  $scope.TotQUN;
                  $scope.currentQyantityfromrequest;
                    $scope.DonorData = true;
                 $scope.getReq_ID = function(reQ_id,Item__ID,Person__Id,ORG_NM,ITM_N,TOTALQUANTITY,NED,PUR,ORGID,NEEDEDBY,REQURSTDT,RECURRENTQUANTITY,CRRQUANTITY)
                 { 
                    // alert(TOTALQUANTITY);
//                     alert(CRRQUANTITY);
                      $scope.personValue = $localStorage.JsonOBJECT;
                      if($scope.personValue == undefined ){
                        alert(' Please login or register to donate...');
                      }
                      else
                      {

                     $scope.currentquantity = RECURRENTQUANTITY;
                     $scope.currentQyantityfromrequest = CRRQUANTITY;
                     $scope.requestsneedeby = NEEDEDBY;
                     $scope.requesteddate = REQURSTDT;
                     $scope.person_IID = Person__Id;
                     $scope.IItem_I = Item__ID;
                     $scope.Req_Id = reQ_id;
                     $scope.ORGName = ORG_NM;
                     $scope.ItemNM = ITM_N;
                     $scope.TotQUN = TOTALQUANTITY;
                     $scope.NEDTime = NED;
                     $scope.PUrPOse = PUR;
                     $scope.ORG__ID = ORGID;;
                    
                     $scope.PAR_Ngo = true;
                     $scope.DonorData = false;
                      var neddeddate= new Date($scope.NEDTime);

                    var NEDTime = new Date(neddeddate).toISOString().split('T')[0];
                            document.getElementsByName("setTodaysDate")[0].setAttribute('min', NEDTime);
                            document.getElementsByName("setTodaysDate1")[0].setAttribute('min', NEDTime);
                      $scope.displyLocDon();
                    }
                 };
                 
        
                  $scope.PAR_Ngo = false;
                  $scope.donorlogin= false;
                  $scope.donorNotLogin = true;
                 $scope.send_back = function()
                 {
                  //   alert($scope.DONSPT);
                      
                      $scope.DONSPT = undefined;
                      $scope.Don_date = undefined;
                      $scope.GQt = undefined;
                     //  alert($scope.DONSPT);
                      $scope.PAR_Ngo = false;
                       $scope.DonorData= true;
                 }
                 $scope.Check_functionw = function()
                {     
                    //alert('inside the check function');
                        $scope.per_son = $localStorage.JsonOBJECT;
                        //alert('value of' +$scope.per_son);
                        if($scope.per_son == undefined)
                        {
                                 $scope.donorlogin= false;
                                 $scope.donorloginback = true;
                                 $scope.donorNotLogin = true;
                            //  alert('Please Register or login!');
                           
                         //    alert('inside Donor_Volunter');
                           // $location.path('/Donor_Volunteer');
                          
                        }
                        else
                        {
                            $scope.per_son = $localStorage.JsonOBJECT.personType.personType;
                                if($scope.per_son == 'DONOR')
                              {
                                  //alert('in the 2nd tier');
                                $scope.donorlogin= true;
                                $scope.donorNotLogin = false;
                                $scope.donorloginback = true;


                              }
                    }
                 
            
           
        };
           $scope.Check_donor = function()
                {     
                    //alert('inside the check function');
                        $scope.perrson = $localStorage.JsonOBJECT;
                        //alert('value of' +$scope.perrson);
                    
                        if($scope.perrson == 'DONOR')
                        {
                          //  alert('jjjj');
//                             $scope.donorlogin= true;
//                              $scope.donorNotLogin = false;
//                           
                         //    alert('inside Donor_Volunter');
                           // $location.path('/Donor_Volunteer');
                          
                        }
                          if($scope.perrson == undefined)
                        {
                             //alert('in the ff 2nd tier');
//                             $scope.donorlogin= false;
//                              $scope.donorNotLogin = true;
                           //alert('Please Register or login!');
                           $location.path('/Donor_Voldfsunteer');
                           
                          // $localStorage.$reset();
                          //localStorage.clear();
                          //$location.path('/hime');
                          
                        }
                 
             document.request.reset();
           
        };

                $scope.displyLocDon =  function()
                {
                    //alert('Ngo Id: '+$scope.person_IID);
                    $scope.perlocation;
                    //console.log($scope.perlocation);
                   
                    for(var nd=0; nd<$scope.perlocation.length;nd++)
                    {
                        $scope.perlocisd = $scope.perlocation[nd].personId;
                        //alert('loc per id:   '+$scope.perlocisd)
                        if($scope.person_IID == $scope.perlocisd)
                        {
                           // alert('loc per id:   '+$scope.perlocisd)
                            $scope.ngloc = $scope.perlocation[nd].locationName + $scope.perlocation[nd].addressLine1 + $scope.perlocation[nd].cityId + $scope.perlocation[nd].stateId; 
                        }
                    }
                };

            
                /*Volunteer functionality for all services*/
                 $scope.volunteeerequestdata = function()
                {
                   // alert('hi');
                      $http({
                              url: requests,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope.requestdatainvolunteer = response.data;
                                  $scope.volunteeepersondonationdata();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                              
                };
                 $scope.volunteeepersondonationdata = function()
                {
                   // alert('hi');
                      $http({
                              url: personDonations,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope.volunteeedonationdata = response.data;
                                  $scope.locationsdatainvolunteer();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                              
                };
                 $scope.locationsdatainvolunteer = function()
                {
                   // alert('hi');
                      $http({
                              url: locations,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope.nvlocationsdatainvolunteer = response.data;
                                  $scope.pickuprequestdatainvolunteer();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                              
                };
                $scope.pickuprequestdatainvolunteer = function()
                {
                   // alert('hi');
                      $http({
                              url: pickupRequest,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope.pickuprequestdata = response.data;
                                    $scope.get_picDonor();
                                  
                                
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                              
                };

                $scope.pickupdisabledate = function()
                {
                    
                    var pickupDeliverydate= new Date($scope.NEDT);
                     var NEDT = new Date(pickupDeliverydate).toISOString().split('T')[0];
                            document.getElementsByName("InpickupDate")[0].setAttribute('min', NEDT);
                };
               
                 $scope.showtable = false;
//                 $scope.Donations_data;
//                 $scope.personDonate;
//                 $scope.perlocation;
                 $scope.get_picDonor = function()
                 { 

                     $scope.perlocation;
                     /*New services data*/
                     //console.log('New services data')
                     //console.log($scope.requestdatainvolunteer);
                     //console.log($scope.volunteeedonationdata);
                     //console.log($scope.nvlocationsdatainvolunteer);
                     //console.log($scope.pickuprequestdata);
                     //console.log('EndNew services data')

                     $scope.VOLN_data = [];
                     for(var i=0; i<$scope.requestdatainvolunteer.data.length; i++)
                     {
                         $scope.Req_itemid = $scope.requestdatainvolunteer.data[i].itemId;
                         $scope.Req_id = $scope.requestdatainvolunteer.data[i].id;
                         for(var pirq = 0; pirq < $scope.pickuprequestdata.data.length; pirq++)
                         {
                             $scope.picrequest_id = $scope.pickuprequestdata.data[pirq].requestId;
                             $scope.picreqauto_id = $scope.pickuprequestdata.data[pirq].id;
                             $scope.voludeliverysta = $scope.pickuprequestdata.data[pirq].requestStatus;
                             if($scope.Req_id == $scope.picrequest_id && ($scope.voludeliverysta == 5 || $scope.voludeliverysta == 1 || $scope.voludeliverysta == 2))
                             {
                                // alert('hi reqid' + $scope.Req_id);
                                // alert('hi pickup req id' + $scope.picrequest_id);
                                 for(var j=0; j<$scope.volunteeedonationdata.data.length; j++)
                                {
                                   // alert('inside of data');
                                    $scope.perdon_itemId = $scope.volunteeedonationdata.data[j].donation.itemId;
                                   // $scope.don_iteId = $scope.Donations_data.data[j].itemId;
                                    $scope.don_value = $scope.volunteeedonationdata.data[j].donation.isDonorDelivering;
                                   // //console.log($scope.don_value.length);
                                  //  alert($scope.don_iteId);

                                    if($scope.Req_itemid == $scope.perdon_itemId && $scope.don_value == 'N')
                                    {
                                      //alert('inside data');
                                     //  $scope.Donor_name = $localStorage.JsonOBJECT.person.fullName;
                                       //$scope.get_Locations($scope.Req_perId,$scope.perDonatin_perId);
                                       $scope.Req_perId = $scope.requestdatainvolunteer.data[i].personId;
                                       $scope.Req_perphone = $scope.requestdatainvolunteer.data[i].person.primaryPhone;
                                       $scope.Req_peremail = $scope.requestdatainvolunteer.data[i].person.email;
                                       $scope.perDonatin_perId = $scope.volunteeedonationdata.data[j].personId;
                                        $scope.donorNaam = $scope.volunteeedonationdata.data[j].person.fullName;
                                        $scope.donorphone = $scope.volunteeedonationdata.data[j].person.primaryPhone;
                                        $scope.donoremail = $scope.volunteeedonationdata.data[j].person.email;
                                        $scope.orgnaam = $scope.requestdatainvolunteer.data[i].org.orgName;
                                        $scope.itemNaam = $scope.requestdatainvolunteer.data[i].item.itemName;
                                        $scope.totqun = $scope.requestdatainvolunteer.data[i].totalQuantityNeeded;
                                        $scope.req_date = $scope.requestdatainvolunteer.data[i].requestDate;
                                        $scope.need_date = $scope.requestdatainvolunteer.data[i].neededBy;
                                        $scope.quntu = $scope.volunteeedonationdata.data[j].donation.quantity;
                                        $scope.don_Date = $scope.volunteeedonationdata.data[j].donation.donationDate;
                                        $scope.don_id = $scope.volunteeedonationdata.data[j].donation.id;
                                        $scope._donationid = $scope.volunteeedonationdata.data[j].id;
                                        $scope.ORG_ID = $scope.requestdatainvolunteer.data[i].orgId;
                                        $scope.REq_ID = $scope.requestdatainvolunteer.data[i].id;
                                        

                                         $scope.Voulen_data = {};
                                        $scope.Voulen_data.D_Name = $scope.donorNaam;
                                        $scope.Voulen_data.D_email = $scope.donoremail;
                                        $scope.Voulen_data.D_phone = $scope.donorphone;
                                        $scope.Voulen_data.Ngo_email = $scope.Req_peremail;
                                        $scope.Voulen_data.Ngo_phone = $scope.Req_perphone;
                                        $scope.Voulen_data.ORG_NAme = $scope.orgnaam;
                                        $scope.Voulen_data.IT_name = $scope.itemNaam;
                                        $scope.Voulen_data.TOT_Qun = $scope.totqun;
                                        $scope.Voulen_data.REQ_date = $scope.req_date;
                                        $scope.Voulen_data.NEED_time = $scope.need_date;
                                        $scope.Voulen_data.Qun_Tity = $scope.quntu;
                                        $scope.Voulen_data.DON_Date = $scope.don_Date;
                                        $scope.Voulen_data.DON_id = $scope.don_id;
                                        $scope.Voulen_data.ORGAS_id = $scope.ORG_ID;
                                        $scope.Voulen_data.ReQq__id = $scope.REq_ID;
                                        $scope.Voulen_data._donation_ID = $scope._donationid;
                                        $scope.Voulen_data._picautoid = $scope.picreqauto_id;
                                        $scope.Voulen_data.delivery_status = $scope.voludeliverysta;

                                        for(var k=0; k<$scope.nvlocationsdatainvolunteer.data.length; k++)
                                         {

                                             if($scope.Req_perId == $scope.nvlocationsdatainvolunteer.data[k].personId)
                                             {
                                               // alert('inside Loca Ngo');
                                                  $scope.Voulen_data.Ngo_loc = $scope.nvlocationsdatainvolunteer.data[k].locationName + $scope.nvlocationsdatainvolunteer.data[k].addressLine1 + $scope.nvlocationsdatainvolunteer.data[k].cityId + $scope.nvlocationsdatainvolunteer.data[k].stateId; 
                                                  //alert('inside Ngo Locations' +$scope.Ngo_location);
                                             }
                                             if($scope.perDonatin_perId == $scope.nvlocationsdatainvolunteer.data[k].personId)
                                             {
                                                  // alert('inside Loca Donor');
                                                   $scope.Voulen_data.Donar_loc  = $scope.nvlocationsdatainvolunteer.data[k].locationName + $scope.nvlocationsdatainvolunteer.data[k].addressLine1 + $scope.nvlocationsdatainvolunteer.data[k].cityId + $scope.nvlocationsdatainvolunteer.data[k].stateId; 
                                                // alert('inside Donor Location:    ' +$scope.Donor_location );
                                             }

                                         }
                                        $scope.VOLN_data.push($scope.Voulen_data);
                                         //console.log($scope.VOLN_data);

                                    }
                                }
                             }
                         }
                         
                     }
                     
                     if($scope.VOLN_data == '')
                     {
                         $scope.showtable = true;
                        $scope.Volun_action = false;
                     }
                     else
                     {
                        $scope.Volun_action = true;
                        
                     }
                    
                 };

                   $scope.Volun_action = true;
                   $scope.Acceptpickup = true;
                   $scope.Deliverpending = false;
                   $scope.DeliverCompleted = false;
                   $scope._pickupautoid;
                   $scope.OI_D;
                   $scope.RI_D;
                   $scope.NEDT;
                   $scope.perdontionid;
                 $scope.push_detais = function(PICAUID,ON,DN,IN,QT,DD,ND,DID,OID,RID,PERDOINID,NGL,DNL,NGOE,NGOPH,DNOEMA,DNOPHN)
                 {
                        $scope.perdontionid = PERDOINID;
                     $scope._pickupautoid = PICAUID;
                     $scope.ONA = ON;
                     $scope.DNA = DN;
                     $scope.INA = IN;
                     $scope.QAT = QT;
                     $scope.DDT = DD;
                     $scope.NEDT = ND;
                     $scope.DI_D = DID;
                     $scope.OI_D = OID;
                     $scope.RI_D = RID;
                     $scope.NGLO = NGL;
                     $scope.DNLO = DNL;
                     $scope.NGOEM = NGOE;
                     $scope.NGOPHN = NGOPH;
                     $scope.DNOPHN = DNOPHN;
                     $scope.DNOEmail = DNOEMA;
                  //  alert($scope.NGLO);
                $scope.pickupdisabledate();

                    $scope.Volun_action = false;
                    $scope.Volun_onload = true;
                   

                 }
                 /*Back button*/
                 $scope.send_backvon = function()
                 {
                    
                    $scope.Volun_action = true;
                    $scope.Volun_onload = false;
                     
                     document.pickup1.reset();
                 }
                 /*Volunteer Accept pickup*/
                     $scope.now=new Date();      
                    $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();
                 $scope.volunteerAccept = function()
                 {
                    // alert('pic req id:  '+$scope._pickupautoid);
                    // alert('Request Id:  '+$scope.RI_D);
                    // alert('Organisation id:'+$scope.OI_D);
                     //alert('person donation id:  '+$scope.perdontionid);
                     $scope.picrequpdatedata = {};
                     $scope.picrequpdatedata.id = '1';
                     $scope.picrequpdatedata.orgId = $scope.OI_D;
                     $scope.picrequpdatedata.requestId = $scope.RI_D;
                     $scope.picrequpdatedata.requestStatus = 2;
                     $scope.picrequpdatedata.donationId = $scope.perdontionid;
                     $scope.picrequpdatedata.createdBy = '1';
                     $scope.picrequpdatedata.createdDatetime = $scope.Curre_date;
                     $scope.picrequpdatedata.lastUpdatedBy = '1';
                     $scope.picrequpdatedata.lastUpdatedDatetime = $scope.Curre_date;
                     //alert(JSON.stringify($scope.picrequpdatedata));
                      $http({
                              url: pickupRequest +'/'+ $scope._pickupautoid,
                              method: "PUT",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.picrequpdatedata
                          
                           }).then(function(response)
                              {
                                  //alert('inside data of Pickup service details...');
                                 //console.log(response.data);
                                   
                                  $scope.pick_details=response.data;
                                  $scope.picreq_ID = $scope.pick_details.data.id;
                                  $scope.pickUp_request($scope.picreq_ID);
                                   document.pickup1.reset();
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                     
//                     $scope._pickupautoid;
//                   $scope.OI_D;
//                   $scope.RI_D;
//                   $scope.perdontionid;
                 };
                 $scope.pickUp_request=function(I_d)
                 {
                     //alert('Pickup request ID:   '+I_d);
                      $scope.user_IID = $localStorage.JsonOBJECT.id;
                    //  alert('User ID    :'+$scope.user_IID);
                     // alert('Quantity:  '+$scope.QAT);
                     // alert($scope.APT);
                     // alert($scope.SPT);
                     // $scope.APTime = new Date($scope.APT);
                      $scope.SPTime = new Date($scope.SPT);
                        $scope.now=new Date();
                        $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                      $scope.JSON_picdetails = {};
                      $scope.JSON_picdetails.id = '1';
                      $scope.JSON_picdetails.pickupRequestId = I_d;
                      $scope.JSON_picdetails.pickupPersonId = $scope.user_IID;
                      $scope.JSON_picdetails.pickupQuantity = $scope.QAT;
                      $scope.JSON_picdetails.scheduledPickupDatetime = $scope.SPTime;
                      $scope.JSON_picdetails.actualPickupDatetime = $scope.SPTime;
                      $scope.JSON_picdetails.createdBy = '1';
                      $scope.JSON_picdetails.createdDatetime = $scope.Curre_date;
                      $scope.JSON_picdetails.lastUpdatedBy = '1';
                      $scope.JSON_picdetails.lastUpdatedDatetime = $scope.Curre_date;
                      //alert('Json Object:  '+ JSON.stringify($scope.JSON_picdetails));
                      /*Service*/
                       $http({
                              url: pickupRequestDetail,
                              method: "POST",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.JSON_picdetails
                          
                           }).then(function(response)
                              {
                                  //alert('inside data of Pickup service details...');
                                 //console.log(response.data);
                                  $scope.Volun_action = true;
                                  $scope.Volun_onload = false;
                                  $scope.volunteeerequestdata();
                                   
                                  
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                     
                 };
                 
                 
                         
              /*Ngo phase for displaying status and updated quantity*/
              
                            $scope.getallReqdetails=function(){
                                
                                $http({
                              url: requests,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                             }).then(function(response)
                              {
                                  //console.log(response);
                                  $scope.allrequests= response.data.data;
                               
                                   $scope.pickuprequests();
                                  
                                  },
                              function(response) 
                              {
                              alert('Error');

                              });
                               var today = new Date().toISOString().split('T')[0];
                            document.getElementsByName("setTodaysDate2")[0].setAttribute('min', today);
                            };
                    $scope.pickuprequests=function(){
                     //   alert('hello');
                     $http({
                              url: pickupRequest,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                        }).then(function(response)
                              {
                                  //console.log(response);
                                 $scope.pickupreqdetails=response.data.data;
                                 //alert('insude request data');
                                  $scope.ngodatafromservices();
    
                              },
                              function(response) 
                              {
                              alert('Error');

                              });  

                };
                  /*onload function in NGO showrequest page*/          
                  $scope.ngodatafromservices = function()
                  {
                    $scope.ngoautoid = $localStorage.JsonOBJECT.id;
                    $scope.pickupreqdetails;
                    $scope.allrequests;
                    //console.log('!!!!!!!!!!!!!!!!!')
                    //console.log($scope.pickupreqdetails);
                    //console.log($scope.allrequests);
                    //console.log('!!!!!!!!!!!!!!!!!');
                    
                    /*Empty Array*/
                    $scope.ngoallrequests = [];
                    for(var ar = 0; ar < $scope.allrequests.length; ar++)
                    {
                        $scope.reqi = $scope.allrequests[ar].id;
                        $scope.requestperon = $scope.allrequests[ar].personId;
                        $scope.reqitemid = $scope.allrequests[ar].itemId;
                        $scope.totalquantiy = $scope.allrequests[ar].totalQuantityNeeded;
                        $scope.CurrentQuantity =  $scope.allrequests[ar].currentQuantityNeeded;
                        
                       if($scope.ngoautoid == $scope.requestperon && $scope.totalquantiy > $scope.CurrentQuantity)
                            {
                                 $scope.allngorequests = {};
                                $scope.allngorequests.ngoreqid = $scope.allrequests[ar].id;
                                $scope.allngorequests.ngoorganisationid = $scope.allrequests[ar].orgId;
                                $scope.allngorequests.ngoitemname = $scope.allrequests[ar].item.itemName;
//                                $scope.allngorequests.Requiredqunatity = $scope.allrequests[ar].item.units;
                                $scope.allngorequests.totalquantity = $scope.allrequests[ar].totalQuantityNeeded;
                                $scope.allngorequests.donatedquantity = $scope.allrequests[ar].currentQuantityNeeded;
                                $scope.allngorequests.ngopurpose = $scope.allrequests[ar].purpose;
                                $scope.allngorequests.ngoneeded = $scope.allrequests[ar].neededBy;
//                                if($scope.allrequests[ar].currentQuantityNeeded == 0)
//                                {
//                                      $scope.allngorequests.donatedquantity = $scope.allrequests[ar].totalQuantityNeeded;
//                                }
//                                else
//                                {
//                                    $scope.allngorequests.donatedquantity = $scope.allrequests[ar].currentQuantityNeeded;
//                                }
                              
                                for(var prd = 0; prd < $scope.pickupreqdetails.length; prd++)
                                {
                                    $scope.pickupreqid = $scope.pickupreqdetails[prd].requestId;
                                    $scope.pickuppersonid = $scope.pickupreqdetails[prd].request.personId;
                                    $scope.pickuppersonitemid = $scope.pickupreqdetails[prd].request.itemId;
                                    if($scope.reqi == $scope.pickupreqid)
                                    {
                                         $scope.allngorequests.ngostatus = $scope.pickupreqdetails[prd].requestStatus;
                                         $scope.allngorequests.pickupreqdonationid = $scope.pickupreqdetails[prd].donationId;
                                        $scope.allngorequests.pickupreqautoid = $scope.pickupreqdetails[prd].id;
                                         
                                    }   
                                }
                               //alert(JSON.stringify($scope.allngorequests));
                                 $scope.ngoallrequests.push($scope.allngorequests);
                         }
                            
                                       
                    }
                        /*Show and hide there is no records*/
                        $scope.acceptbtnclick = true;
                            if($scope.ngoallrequests == '')
                             {
                              
                                $scope.withoutreqdata = true;
                                $scope.withreqdata = false;
                                $scope.ngoreq = true;
                                $scope.NGO_table = false;
                                $scope.requestbtn = true;
                                $scope.acceptbtnclick = false;

                             }
                             else
                             {
                                $scope.NGO_table = true;
                                 $scope.withreqdata = true;
                                 $scope.withoutreqdata = false;
                                $scope.requestbtn = false;
                                 $scope.ngoreq = false;
                                 $scope.onacceptbtnclick = true;
                             }   
                  };
                   $scope.withreqdata = false;
                   $scope.withoutreqdata = false;

                  /*Ngo Accept Request*/
                  $scope.acceptrequest = function(ngoreqid,ngoorganisationid,pickupreqdonationid,pickupreqautoid)
                  {
                 
                      $scope.JSON_picng0details = {};
                      $scope.JSON_picng0details.id = '1';
                     $scope.JSON_picng0details.orgId = ngoorganisationid;
                     $scope.JSON_picng0details.requestId = ngoreqid;
                     $scope.JSON_picng0details.requestStatus = 4;
                     $scope.JSON_picng0details.donationId = pickupreqdonationid;         
                     $scope.JSON_picng0details.createdBy = '1';
                      $scope.JSON_picng0details.createdDatetime = $scope.Curre_date;
                      $scope.JSON_picng0details.lastUpdatedBy = '1';
                      $scope.JSON_picng0details.lastUpdatedDatetime = $scope.Curre_date;
                   //   alert(JSON.stringify($scope.JSON_picng0details));
                       $http({
                              url: pickupRequest+'/'+pickupreqautoid,
                              method: "PUT",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.JSON_picng0details
                          
                           }).then(function(response)
                              {
                                //  alert('inside data of updated Pickup service details...');
                                 //console.log(response.data);
                                 $scope.getallReqdetails();
                                   
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                      
                  };
                 
                 
                 
 
                    /*Donor Pahse All Functionality part 04/12/2017 by RamakrishnaD*/
                    
            
                 /*Requests in DONOR*/
                 $scope.ngoreqiestsindonor = function()
                 {
                            $http({
                              url: requests,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope._ngorequestsindonor = response.data.data;
                              $scope.ngopickupreqiestsindonor();
                                 // $scope.getdeliverstatus();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                 /*Pick RequestDetails*/
                 $scope.ngopickupreqiestsindonor = function()
                 {
                            $http({
                              url: pickupRequest,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope._ngopickuprequestsindonor = response.data;
                              $scope.ngopersondonationsindonor();
                                  //$scope.getdeliverstatus();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                 /*PersonDonations in Donor*/
                  $scope.ngopersondonationsindonor = function()
                 {
                            $http({
                              url: personDonations,
                              method: "GET",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             }
                           }).then(function(response)
                              {
                                  $scope._ngopersondonationsindonor = response.data;
                              
                                $scope.ngolocationindonor();
                                   },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                   $scope.ngolocationindonor = function()
                 {
                     $http({
                              url: locations,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                           }).then(function(response)
                              {
                                  $scope.perlocation = response.data.data;
                                  
                                 $scope.ngoPickupRequestDetails();
                                      //console.log($scope.perlocation);
                                     $scope.displyLocDon();
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                  $scope.ngoPickupRequestDetails = function()
                 {
                     $http({
                              url: pickupRequestDetail,
                              method: "GET",
                              dataType: "json",
                              headers: {
                              'Content-Type' :'application/json'
                             }                           
                           }).then(function(response)
                              {
                                  $scope.PersonPickupReqDetail = response.data.data;
                                  
                                  //console.log($scope.PersonPickupReqDetail);
                                   $scope.getdeliverstatusss();
                                 $scope.displyLocDon();
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
              /*Displaying data in  twi roles phase*/

                    $scope.getdeliverstatusss = function()
                {

                   $scope._ngorequestsindonor;
                   $scope._ngopickuprequestsindonor;
                   $scope._ngopersondonationsindonor;
                   $scope.perlocation;
                   $scope.PersonPickupReqDetail;
                    //console.log($scope.PersonPickupReqDetail);
                    $scope.donoronloaddata = [];
                    $scope.person_id = $localStorage.JsonOBJECT;

                    if(!$scope.person_id)
                    {
//                        alert('in side undefined ');
                          for(var reqd = 0; reqd<$scope._ngorequestsindonor.length; reqd++)
                        {
                         $scope.reqid = $scope._ngorequestsindonor[reqd].id;
                                $scope.reqitemid = $scope._ngorequestsindonor[reqd].itemId;

                                $scope.loopdata = {};
                                $scope.loopdata.id = '1';
                                $scope.loopdata.req_id = $scope.reqid;
                                $scope.loopdata.ngoname = $scope._ngorequestsindonor[reqd].org.orgName;
                                 $scope.ngoPerId = $scope._ngorequestsindonor[reqd].personId;
                                $scope.loopdata.ngoid = $scope.ngoPerId;
                                $scope.loopdata.ngocontact = $scope._ngorequestsindonor[reqd].person.primaryPhone;
                                $scope.loopdata.ngoitemid = $scope._ngorequestsindonor[reqd].itemId;
                                $scope.loopdata.ngoorgid = $scope._ngorequestsindonor[reqd].orgId;
                                $scope.loopdata.itemname = $scope._ngorequestsindonor[reqd].item.itemName;
//                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].item.units;
                               $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                $scope.loopdata.currentquantityneeded = $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                $scope.loopdata.ngopurpose = $scope._ngorequestsindonor[reqd].purpose;
                                $scope.loopdata.neededby = $scope._ngorequestsindonor[reqd].neededBy;
                                $scope.loopdata.requesteddt = $scope._ngorequestsindonor[reqd].requestDate;

                                for(var perlocat = 0;perlocat < $scope.perlocation.length;perlocat++)
                                        {

                                          if($scope.ngoPerId == $scope.perlocation[perlocat].personId)
                                        {
                                          $scope.loopdata.NgoAddress = $scope.perlocation[perlocat].addressLine1+','+$scope.perlocation[perlocat].locationName+','+$scope.perlocation[perlocat].cityId+','+$scope.perlocation[perlocat].stateId;
                                        }
                                      }
                               // $scope.loopdata.NgoAddress = ngolocationindonor($scope.loopdata.ngoid);


//                                 $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                  if($scope._ngorequestsindonor[reqd].currentQuantityNeeded == 0)
                                    {
//                                            alert('in side if ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                    }
                                    else
                                    {
//                                            alert('in side else ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                    }
                            for(var prd = 0;prd<$scope._ngopickuprequestsindonor.data.length;prd++)
                              {

                                  //alert($scope.deliverysta);
                                  $scope.pireq_reqid = $scope._ngopickuprequestsindonor.data[prd].requestId;
                                  $scope.picReqId = $scope._ngopickuprequestsindonor.data[prd].id;
                                  if($scope.reqid == $scope.pireq_reqid)
                                  {
                                  //  alert('hi inside checking for delivery status');
                                   for(var perlocat = 0;perlocat < $scope.perlocation.length;perlocat++)
                                        {

                                       
                                        if($scope.DonationPersonId == $scope.perlocation[perlocat].personId)
                                        {
                                          $scope.loopdata.DonorAddress = $scope.perlocation[perlocat].addressLine1+','+$scope.perlocation[perlocat].locationName+','+$scope.perlocation[perlocat].cityId+','+$scope.perlocation[perlocat].stateId;
                                        }
                                     //console.log($scope.reqid+"..............."+$scope.pireq_reqid);
                               for(var pd=0;pd< $scope._ngopersondonationsindonor.data.length;pd++)
                                  {
                                       $scope.DonationPersonId = $scope._ngopersondonationsindonor.data[pd].personId;
                                       $scope.isDonorDelivering = $scope._ngopersondonationsindonor.data[pd].donation.isDonorDelivering;
                                       $scope.pireq_personitemid = $scope._ngopersondonationsindonor.data[pd].donation.itemId;
                                    

                                    if($scope.pireq_personitemid == $scope.reqitemid) {
                                    //if($scope.DonationPersonId != undefined)
                                    

                                        // Search in Locations Table for the matching $scope.pireq_personid
                                         $scope.loopdata.donorName = $scope._ngopersondonationsindonor.data[pd].person.fullName;
                                         $scope.loopdata.donorNumber = $scope._ngopersondonationsindonor.data[pd].person.primaryPhone;
                                        $scope.loopdata.donorEmail = $scope._ngopersondonationsindonor.data[pd].person.email;
                                         
                                        /*for(var perlocat = 0;perlocat < $scope.perlocation.length;perlocat++)
                                        {

                                     
                                        if($scope.DonationPersonId == $scope.perlocation[perlocat].personId)
                                        {
                                          $scope.loopdata.DonorAddress = $scope.perlocation[perlocat].addressLine1+','+$scope.perlocation[perlocat].locationName+','+$scope.perlocation[perlocat].cityId+','+$scope.perlocation[perlocat].stateId;
                                        }*/
                                        
                                       // $scope.loopdata.DonorAddress = ngolocationindonor($scope.DonationPersonId);
                                        for(var preqd=0;preqd< $scope.PersonPickupReqDetail.length;preqd++)
                                        {

                                            $scope.PickupRequestPerson = $scope.PersonPickupReqDetail[preqd].pickupPersonId;
                                            $scope.PickupRequestIdInPicDetail = $scope.PersonPickupReqDetail[preqd].pickupRequestId;
                                            /*When Donor only logged*/
                                            if($scope.DonationPersonId == $scope.perid)
                                            {
                                              /*and same donor will be a Volunteer*/
                                              //console.log('Donor');
                                               if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                     $scope.loopdata.deliverystatus = '8';
                                                  }
                                              if($scope.PickupRequestPerson == $scope.perid && $scope.PickupRequestIdInPicDetail == $scope.picReqId)
                                                {
                                                  //console.log('donor is a pickup person');
                                                  
                                                  if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '3')
                                                  {
                                                    $scope.loopdata.deliverystatus = '7';
                                                  }
                                                                                               
                                                  $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                                  $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].
                                                  id;
                                                  $scope.loopdata.PickupPerson = $scope.PickupRequestPerson;
                                                }

                                                else
                                                {
                                                  //console.log('Not pickup person');
                                                  if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                    $scope.loopdata.deliverystatus = '8';
                                                  }
                                                  $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                                  $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                                }
                                            }/*Not Donor */
                                            else
                                            {
                                              //console.log('Not Donor');
                                              if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                     $scope.loopdata.deliverystatus = '6';
                                                     
                                                  }

                                              // $scope.loopdata.deliverystatus = $scope._ngopickuprequestsindonor.data[prd].requestStatus;
                                              $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                              $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                              $scope.loopdata.PickupPerson = $scope.PickupRequestPerson;
                                            }
                                     
                                        }/*for loop PersonPickupReqDetail*/
                                       }   /*for loop Person Locations*/
                                       
                                      
                                  }
                                 /* else
                                  {
                                     $scope.loopdata.DonorAddress = 'No one Donate now';
                                   
                                  }*/

                            }
              }
              }
              
                             $scope.donoronloaddata.push($scope.loopdata);
                             //console.log($scope.donoronloaddata);
                         }
                     /*Show there is no records*/
                            if($scope.donoronloaddata == ''){
                               $scope.showreq = true;
                               $scope.DonorData = false;
                           }
                           else{
                           $scope.DonorData = true;
                           $scope.showreq = false;
                           }
                    }
                    else
                    {
                        $scope.perid = $localStorage.JsonOBJECT.id;
                        for(var reqd = 0; reqd<$scope._ngorequestsindonor.length; reqd++)
                        {
                         $scope.reqid = $scope._ngorequestsindonor[reqd].id;
                                $scope.reqitemid = $scope._ngorequestsindonor[reqd].itemId;

                                $scope.loopdata = {};
                                $scope.loopdata.id = '1';
                                $scope.loopdata.req_id = $scope.reqid;
                                $scope.loopdata.ngoname = $scope._ngorequestsindonor[reqd].org.orgName;
                                 $scope.ngoPerId = $scope._ngorequestsindonor[reqd].personId;
                                $scope.loopdata.ngoid = $scope.ngoPerId;
                                $scope.loopdata.ngocontact = $scope._ngorequestsindonor[reqd].person.primaryPhone;
                                $scope.loopdata.ngoitemid = $scope._ngorequestsindonor[reqd].itemId;
                                $scope.loopdata.ngoorgid = $scope._ngorequestsindonor[reqd].orgId;
                                $scope.loopdata.itemname = $scope._ngorequestsindonor[reqd].item.itemName;
//                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].item.units;
                               $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                $scope.loopdata.currentquantityneeded = $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                $scope.loopdata.ngopurpose = $scope._ngorequestsindonor[reqd].purpose;
                                $scope.loopdata.neededby = $scope._ngorequestsindonor[reqd].neededBy;
                                $scope.loopdata.requesteddt = $scope._ngorequestsindonor[reqd].requestDate;

                                for(var perlocat = 0;perlocat < $scope.perlocation.length;perlocat++)
                                        {

                                          if($scope.ngoPerId == $scope.perlocation[perlocat].personId)
                                        {
                                          $scope.loopdata.NgoAddress = $scope.perlocation[perlocat].addressLine1+','+$scope.perlocation[perlocat].locationName+','+$scope.perlocation[perlocat].cityId+','+$scope.perlocation[perlocat].stateId;
                                        }
                                      }
                               // $scope.loopdata.NgoAddress = ngolocationindonor($scope.loopdata.ngoid);


//                                 $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                  if($scope._ngorequestsindonor[reqd].currentQuantityNeeded == 0)
                                    {
//                                            alert('in side if ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                    }
                                    else
                                    {
//                                            alert('in side else ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                    }
                            for(var prd = 0;prd<$scope._ngopickuprequestsindonor.data.length;prd++)
                              {

                                  //alert($scope.deliverysta);
                                  $scope.pireq_reqid = $scope._ngopickuprequestsindonor.data[prd].requestId;
                                  $scope.picReqId = $scope._ngopickuprequestsindonor.data[prd].id;
                                  if($scope.reqid == $scope.pireq_reqid)
                                  {
                                  //  alert('hi inside checking for delivery status');
                                  for(var perlocat = 0;perlocat < $scope.perlocation.length;perlocat++)
                                        {

                                       
                                        if($scope.DonationPersonId == $scope.perlocation[perlocat].personId)
                                        {
                                          $scope.loopdata.DonorAddress = $scope.perlocation[perlocat].addressLine1+','+$scope.perlocation[perlocat].locationName+','+$scope.perlocation[perlocat].cityId+','+$scope.perlocation[perlocat].stateId;
                                        }
                                     //console.log($scope.reqid+"..............."+$scope.pireq_reqid);
                               for(var pd=0;pd< $scope._ngopersondonationsindonor.data.length;pd++)
                                  {
                                       $scope.DonationPersonId = $scope._ngopersondonationsindonor.data[pd].personId;
                                       $scope.isDonorDelivering = $scope._ngopersondonationsindonor.data[pd].donation.isDonorDelivering;
                                       $scope.pireq_personitemid = $scope._ngopersondonationsindonor.data[pd].donation.itemId;
                                    

                                    if($scope.pireq_personitemid == $scope.reqitemid) {

                                        // Search in Locations Table for the matching $scope.pireq_personid
                                         $scope.loopdata.donorName = $scope._ngopersondonationsindonor.data[pd].person.fullName;
                                        $scope.loopdata.donorNumber = $scope._ngopersondonationsindonor.data[pd].person.primaryPhone;
                                        $scope.loopdata.donorEmail = $scope._ngopersondonationsindonor.data[pd].person.email;
                                        

                                       // $scope.loopdata.DonorAddress = ngolocationindonor($scope.DonationPersonId);
                                        for(var preqd=0;preqd< $scope.PersonPickupReqDetail.length;preqd++)
                                        {

                                            $scope.PickupRequestPerson = $scope.PersonPickupReqDetail[preqd].pickupPersonId;
                                            $scope.PickupRequestIdInPicDetail = $scope.PersonPickupReqDetail[preqd].pickupRequestId;
                                            /*When Donor only logged*/
                                            if($scope.DonationPersonId == $scope.perid)
                                            {
                                              /*and same donor will be a Volunteer*/
                                              //console.log('Donor');
                                               if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                     $scope.loopdata.deliverystatus = '8';
                                                  }
                                              if($scope.PickupRequestPerson == $scope.perid && $scope.PickupRequestIdInPicDetail == $scope.picReqId)
                                                {
                                                  //console.log('donor is a pickup person');
                                                  
                                                  if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '3')
                                                  {
                                                    $scope.loopdata.deliverystatus = '7';
                                                  }
                                                                                               
                                                  $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                                  $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].
                                                  id;
                                                  $scope.loopdata.PickupPerson = $scope.PickupRequestPerson;
                                                }

                                                else
                                                {
                                                  //console.log('Not pickup person');
                                                  if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                    $scope.loopdata.deliverystatus = '8';
                                                  }
                                                  $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                                  $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                                }
                                            }/*Not Donor */
                                            else
                                            {
                                              //console.log('Not Donor');
                                              if($scope._ngopickuprequestsindonor.data[prd].requestStatus == '1')
                                                  {
                                                     $scope.loopdata.deliverystatus = '6';
                                                     
                                                  }

                                              // $scope.loopdata.deliverystatus = $scope._ngopickuprequestsindonor.data[prd].requestStatus;
                                              $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                              $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                              $scope.loopdata.PickupPerson = $scope.PickupRequestPerson;
                                            }
                                     
                                        }/*for loop PersonPickupReqDetail*/
                                       }   /*for loop Person Locations*/
                                       }/*  $scope.pireq_personitemid == $scope.reqitemid*/

                                   }

                                  //   alert(JSON.stringify($scope.loopdata ));

                                  }
                           

                            }
                             $scope.donoronloaddata.push($scope.loopdata);
                             //console.log($scope.donoronloaddata);
                         }
                        /*Show there is no records*/
                          if($scope.donoronloaddata == ''){
                               $scope.showreq = true;
                               $scope.DonorData = false;
                           }
                           else{
                           $scope.DonorData = true;
                           $scope.showreq = false;
                           }
                    }
                };




/*Latest code*/
                $scope.getdeliverstatussts = function()
                {

                   $scope._ngorequestsindonor;
                   $scope._ngopickuprequestsindonor;
                   $scope._ngopersondonationsindonor;
                    $scope.donoronloaddata = [];
                    //console.log($scope._ngorequestsindonor);
                    //console.log($scope._ngopickuprequestsindonor);
                    //console.log($scope._ngopersondonationsindonor);
                    $scope.person_id = $localStorage.JsonOBJECT;
                   // alert('local value:   ' + $scope.person_id);
                    
                    if(!$scope.person_id)
                    {
//                        alert('in side undefined ');
                        for(var reqd = 0; reqd<$scope._ngorequestsindonor.length; reqd++)
                        {
                       //                        alert($scope._ngorequestsindonor[reqd].currentQuantityNeeded);
                                $scope.reqid = $scope._ngorequestsindonor[reqd].id;
                                $scope.reqitemid = $scope._ngorequestsindonor[reqd].itemId;

                                $scope.loopdata = {};
                                $scope.loopdata.id = '1';
                                $scope.loopdata.req_id = $scope.reqid;
                                $scope.loopdata.ngoname = $scope._ngorequestsindonor[reqd].org.orgName;
                                $scope.loopdata.ngoid = $scope._ngorequestsindonor[reqd].personId;
                                $scope.loopdata.ngoitemid = $scope._ngorequestsindonor[reqd].itemId;
                                $scope.loopdata.ngoorgid = $scope._ngorequestsindonor[reqd].orgId;
                                $scope.loopdata.itemname = $scope._ngorequestsindonor[reqd].item.itemName;
//                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].item.units;
                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                $scope.loopdata.currentquantityneeded = $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                $scope.loopdata.ngopurpose = $scope._ngorequestsindonor[reqd].purpose;
                                $scope.loopdata.neededby = $scope._ngorequestsindonor[reqd].neededBy;
                                $scope.loopdata.requesteddt = $scope._ngorequestsindonor[reqd].requestDate;
//                                 $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;;
                               
                                $scope.loopdata.deliverystatusngo = '5';
                                if($scope._ngorequestsindonor[reqd].currentQuantityNeeded == 0)
                                    {
                                      //      alert('in side if ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                    }
                                    else
                                    {
                                         //   alert('in side else ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                    }

                        for(var prd = 0;prd<$scope._ngopickuprequestsindonor.data.length;prd++)
                        {
                            
                            $scope.pireq_reqid = $scope._ngopickuprequestsindonor.data[prd].requestId;

                            if($scope.reqid == $scope.pireq_reqid)
                            {
                         //  alert('hi inside checking for delivery status');
                               //console.log($scope.reqid+"..............."+$scope.pireq_reqid);
                               for(var pd=0;pd< $scope._ngopersondonationsindonor.data.length;pd++)
                            {
                                 $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                 $scope.pireq_personid = $scope._ngopersondonationsindonor.data[pd].personId;
                                 $scope.pireq_personitemid = $scope._ngopersondonationsindonor.data[pd].donation.itemId;
                                 
                                 if($scope.pireq_personitemid == $scope.reqitemid )
                                 {
                                    // alert('raja the great');
                                     //console.log($scope.pireq_personitemid+"##########"+$scope.reqitemid);
                                     $scope.deliverysta = $scope._ngopickuprequestsindonor.data[prd].requestStatus;
                                     $scope.loopdata.deliverystatus = $scope.deliverysta;
                                     //console.log($scope.pireq_personitemid+"*************"+$scope.deliverysta);
                                     //break;
                                 }
                             }
                           // alert(JSON.stringify($scope.loopdata ));
                            }
                        }
                        $scope.donoronloaddata.push($scope.loopdata);
//                        alert(JSON.)
                    }
                     /*Show there is no records*/
                            if($scope.donoronloaddata == ''){
                               $scope.showreq = true;
                               $scope.DonorData = false;
                           }
                           else{
                           $scope.DonorData = true;
                           $scope.showreq = false;
                           }
                    }
                    else
                    {
                        $scope.perid = $localStorage.JsonOBJECT.id;
                        for(var reqd = 0; reqd<$scope._ngorequestsindonor.length; reqd++)
                        {
                       // alert('inside for loop');
                         $scope.reqid = $scope._ngorequestsindonor[reqd].id;
                                $scope.reqitemid = $scope._ngorequestsindonor[reqd].itemId;

                                $scope.loopdata = {};
                                $scope.loopdata.id = '1';
                                $scope.loopdata.req_id = $scope.reqid;
                                $scope.loopdata.ngoname = $scope._ngorequestsindonor[reqd].org.orgName;
                                $scope.loopdata.ngoid = $scope._ngorequestsindonor[reqd].personId;
                                $scope.loopdata.ngoitemid = $scope._ngorequestsindonor[reqd].itemId;
                                $scope.loopdata.ngoorgid = $scope._ngorequestsindonor[reqd].orgId;
                                $scope.loopdata.itemname = $scope._ngorequestsindonor[reqd].item.itemName;
//                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].item.units;
                                $scope.loopdata.requiredquantity = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                $scope.loopdata.currentquantityneeded = $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                $scope.loopdata.ngopurpose = $scope._ngorequestsindonor[reqd].purpose;
                                $scope.loopdata.neededby = $scope._ngorequestsindonor[reqd].neededBy;
                                $scope.loopdata.requesteddt = $scope._ngorequestsindonor[reqd].requestDate;
//                                 $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                $scope.loopdata.deliverystatusngo = '5';
                                  if($scope._ngorequestsindonor[reqd].currentQuantityNeeded == 0)
                                    {
//                                            alert('in side if ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded;
                                    }
                                    else
                                    {
//                                            alert('in side else ');
                                        $scope.loopdata.remainingquantityneeded = $scope._ngorequestsindonor[reqd].totalQuantityNeeded - $scope._ngorequestsindonor[reqd].currentQuantityNeeded;
                                    }
                            for(var prd = 0;prd<$scope._ngopickuprequestsindonor.data.length;prd++)
                              {

                                  //alert($scope.deliverysta);
                                  $scope.pireq_reqid = $scope._ngopickuprequestsindonor.data[prd].requestId;
                                  if($scope.reqid == $scope.pireq_reqid)
                                  {
                                  //  alert('hi inside checking for delivery status');
                                     //console.log($scope.reqid+"..............."+$scope.pireq_reqid);
                               for(var pd=0;pd< $scope._ngopersondonationsindonor.data.length;pd++)
                                  {

                                       $scope.pireq_personid = $scope._ngopersondonationsindonor.data[pd].personId;
                                       $scope.pireq_personitemid = $scope._ngopersondonationsindonor.data[pd].donation.itemId;
                                       if($scope.pireq_personitemid == $scope.reqitemid && $scope.pireq_personid==$scope.perid)
                                       {


                                           //console.log($scope.pireq_personitemid+"##########"+$scope.reqitemid);
                                           $scope.deliverysta = $scope._ngopickuprequestsindonor.data[prd].requestStatus;
                                           $scope.loopdata.pickuprequestid = $scope._ngopickuprequestsindonor.data[prd].id;
                                           $scope.loopdata.deliverystatus = $scope.deliverysta;
                                           $scope.loopdata.persondonid = $scope._ngopersondonationsindonor.data[pd].id;
                                           //console.log($scope.pireq_personitemid+"*************"+$scope.deliverysta);
                                           //break;
                                       }
                                   }

                                     // alert(JSON.stringify($scope.loopdata ));

                                  }

                            }
                             $scope.donoronloaddata.push($scope.loopdata);
                         }
                        /*Show there is no records*/
                          if($scope.donoronloaddata == ''){
                               $scope.showreq = true;
                               $scope.DonorData = false;
                           }
                           else{
                           $scope.DonorData = true;
                           $scope.showreq = false;
                           }
                    }
                };

                  //update request from Donor for Accepting Volunteer by Donor

                  $scope.acceptvolunteer = function(picid,requestid,organisationid,donationid)
                  {
                    //alert('hi');
                   // alert('pickup request id:  '+picid);
                  //  alert('Request id:  '+requestid);
                   // alert('organisationid id:  '+organisationid);
                   // alert(donationid);
                    $scope.acceptvolunteerdata = {};
                    $scope.acceptvolunteerdata.orgId = organisationid;
                    $scope.acceptvolunteerdata.requestId = requestid;
                     $scope.acceptvolunteerdata.requestStatus = '5';
                     $scope.acceptvolunteerdata.donationId = donationid;
                     $scope.acceptvolunteerdata.createdBy = '1';
                    $scope.acceptvolunteerdata.createdDatetime = $scope.Curre_date;
                    $scope.acceptvolunteerdata.lastUpdatedBy = '1';
                    $scope.acceptvolunteerdata.lastUpdatedDatetime = $scope.Curre_date;
           //        alert(JSON.stringify( $scope.acceptvolunteerdata));

                   //alert(JSON.stringify($scope._updaterequests));
                  $http({
                              url: pickupRequest+'/'+picid,
                              method: "PUT",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.acceptvolunteerdata
                             }).then(function(response)
                              {
                              //  alert('Hello inside Accept ');
                            },
                         function(response) 
                              {
                              alert('Error');

                              });

                  }





                
                  /*Updating Requets in requests table from the Donor Phase*/
                 $scope.updaterequests = function()
                {
//                   alert($scope.currentQyantityfromrequest);
                    if($scope.currentQyantityfromrequest == 0)
                    {
//                        alert('inside undefined');
                        $scope.ramaining_quantity = $scope.GQt;
                    }
                    else
                    {
//                        alert('Inside data:  '+$scope.currentQyantityfromrequest);
                        $scope.ramaining_quantity = $scope.currentQyantityfromrequest + $scope.GQt;
                    }
                 //alert($scope.ramaining_quantity);
                  $scope._updaterequests = {};
                  $scope._updaterequests.id = '1';
                  $scope._updaterequests.itemId = $scope.IItem_I;
                 // $scope._updaterequests.itemId = $scope.Req_Id;
                  $scope._updaterequests.orgId = $scope.ORG__ID;
                  $scope._updaterequests.personId = $scope.person_IID;
                  $scope._updaterequests.purpose = $scope.PUrPOse;
                  $scope._updaterequests.totalQuantityNeeded = $scope.TotQUN;
                  $scope._updaterequests.currentQuantityNeeded = $scope.ramaining_quantity;
                  $scope._updaterequests.createdBy = '1';
                  $scope._updaterequests.createdDatetime = $scope.Curre_date;
                  $scope._updaterequests.lastUpdatedBy = '1';
                  $scope._updaterequests.lastUpdatedDatetime = $scope.Curre_date;
                  $scope._updaterequests.requestDate = $scope.requesteddate;
                  $scope._updaterequests.neededBy = $scope.requestsneedeby;
                //alert(JSON.stringify($scope._updaterequests));
                  $http({
                              url: requests+'/'+$scope.Req_Id,
                              method: "PUT",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope._updaterequests
                             }).then(function(response)
                              {
                                $scope.Donate();
                         },
                         function(response) 
                              {
                              alert('Error');

                              });
              };
              
                  $scope.don_quatity;
                 $scope.Donate = function()
                 {
                      $scope.don__date = new Date($scope.Don_date);
                      if($scope.PICK == true)
                      {
                         // alert('hello');
                          $scope.pickup = 'N';
                      }
                      else
                      {
                        //  alert('Hai');
                          $scope.pickup = 'Y';
                      }
                      $scope.now=new Date();
                      $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                      $scope.don_data = {};
                      $scope.don_data.id = '1';
                      $scope.don_data.itemId = $scope.IItem_I;
                      $scope.don_data.quantity = $scope.GQt;
                      $scope.don_data.donationDate = $scope.don__date;
                      $scope.don_data.isDonorDelivering = $scope.pickup;
                      $scope.don_data.createdBy = '1';
                      $scope.don_data.createdDatetime = $scope.Curre_date;
                      $scope.don_data.lastUpdatedBy = '1';
                      $scope.don_data.lastUpdatedDatetime = $scope.Curre_date;
                     // alert(JSON.stringify($scope.don_data));
                      
                      
                      $http({
                      url: donations,
                      method: "POST",
                      dataType: "json",

                      headers: {
                        'Content-Type' :'application/json'
                       },
                      data :$scope.don_data
                        
                         }).then(function(response) {
                      //  alert('In side Donationsss.....');
                        //console.log(response);
                        $scope.Donati_id = response.data;
                        $scope.don_ID = $scope.Donati_id.data.id; 
                        $scope.don_quatity = $scope.Donati_id.data.quantity; 
                       
                        $scope.Person_donations($scope.don_ID);
                       // document.request.reset();
                        
                      },
                      function(response) {
                        alert('Errorrrrrrrrrrrrrr');

                      });
                 };
                 
                  /*Send data to person Donations table*/
                 $scope.Person_donations = function(Don_id)
                 {
                    // alert(Don_id);
                   $scope.DOnorper_id = $localStorage.JsonOBJECT.id;
                  // alert($scope.DOnorper_id);
                   $scope.now=new Date();
                   $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                   $scope.per_donation = {};
                   $scope.per_donation.id = '1';
                   $scope.per_donation.donationId = Don_id ;
                   $scope.per_donation.personId = $scope.DOnorper_id ;
                   $scope.per_donation.createdBy = '1' ;
                   $scope.per_donation.createdDatetime = $scope.Curre_date ;
                   $scope.per_donation.lastUpdatedBy = '1' ;
                   $scope.per_donation.lastUpdatedDatetime = $scope.Curre_date ;
                 //  alert(JSON.stringify($scope.per_donation));
                    $http({
                      url: personDonations,
                      method: "POST",
                      dataType: "json",

                      headers: {
                        'Content-Type' :'application/json'
                       },
                      data :$scope.per_donation
                        
                         }).then(function(response) {
                    //    alert('In side person Donationsss.....');
                       
                        $scope.person_Donations = response.data;
                        $scope.dondonationid = $scope.person_Donations.data.id;
                         //console.log($scope.person_Donations);
                        // alert($scope.dondonationid);
                         $scope.Req_Id;
                         $scope.ORG__ID;
                         if($scope.PICK == true)
                            {
                                $scope.pickup_Requestindonor($scope.dondonationid);
                            }
                            else
                            {
                                $scope.Donordelivery($scope.dondonationid);
                            }

                      },
                      function(response) {
                        alert('Errorrrrrrrrrrrrrr');

                      })
                 };
                 
                                 
                 /*send pickup Request for Donor*/
                 $scope.pickup_Requestindonor = function(dondontid)
                 {
                   //  alert('insode pickup requests');
                   //  alert('Requests ID:   '+$scope.Req_Id);
                    // alert('Organisation ID:   '+$scope.ORG__ID);
                     //alert('Donation ID:   '+dondontid);
                     $scope.now=new Date();
                     $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                     $scope.DonPICK_JSon = {};
                     $scope.DonPICK_JSon.id = '1',
                     $scope.DonPICK_JSon.orgId = $scope.ORG__ID;
                     $scope.DonPICK_JSon.requestId = $scope.Req_Id;
                     $scope.DonPICK_JSon.requestStatus = 1;
                     $scope.DonPICK_JSon.donationId = dondontid;
                     $scope.DonPICK_JSon.createdBy = '1';
                     $scope.DonPICK_JSon.createdDatetime = $scope.Curre_date;
                     $scope.DonPICK_JSon.lastUpdatedBy = '1';
                     $scope.DonPICK_JSon.lastUpdatedDatetime = $scope.Curre_date;
                   
                    $http({
                              url: pickupRequest,
                              method: "POST",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.DonPICK_JSon
                          
                           }).then(function(response)
                              {
                           //    alert('inside data of Pickup Request service');
                                  //console.log(response.data);
                                  $scope.PAR_Ngo = false;
                                  $scope.DonorData = true;
                                  $scope.ngoreqiestsindonor();
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                 
                 /*Donor is Volunteer*/
                  $scope.Donordelivery = function(dondontid)
                 {
                     $scope.now=new Date();
                     $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                     $scope.DonPICK_JSon = {};
                     $scope.DonPICK_JSon.id = '1',
                     $scope.DonPICK_JSon.orgId = $scope.ORG__ID;
                     $scope.DonPICK_JSon.requestId = $scope.Req_Id;
                     $scope.DonPICK_JSon.requestStatus = 3;
                     $scope.DonPICK_JSon.donationId = dondontid;
                     $scope.DonPICK_JSon.createdBy = '1';
                     $scope.DonPICK_JSon.createdDatetime = $scope.Curre_date;
                     $scope.DonPICK_JSon.lastUpdatedBy = '1';
                     $scope.DonPICK_JSon.lastUpdatedDatetime = $scope.Curre_date;
                    /*Http service*/
                    $http({
                              url: pickupRequest,
                              method: "POST",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.DonPICK_JSon
                          
                           }).then(function(response)
                              {
                                 // alert('inside data of Pickup service');
                                  //console.log(response.data);
                                  $scope.donpicreqest = response.data;
                                  $scope.donorpicreqid = $scope.donpicreqest.data.id;
                                   $scope.donpicupreqdt($scope.donorpicreqid);
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
                 
              
                 /*send picrequestdetails from Donor*/
                 $scope.donpicupreqdt = function(donpicid)
                 {
                     //alert('inside pickup request details');
                      $scope.donorID = $localStorage.JsonOBJECT.id; 
                      // alert('Donor ID:   '+$scope.donorID);
                      
                      //alert('Donor pickup request ID:   '+donpicid);
                      // alert('Donating Quantiy:    '+$scope.don_quatity);
                       $scope.SPTTIME = new Date($scope.DONSPT);
                       $scope.now=new Date();
                        $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                       $scope.picreqsdata = {};
                      $scope.picreqsdata.id = '1';
                      $scope.picreqsdata.pickupRequestId = donpicid;
                      $scope.picreqsdata.pickupPersonId = $scope.donorID;
                      $scope.picreqsdata.pickupQuantity = $scope.don_quatity;
                      $scope.picreqsdata.scheduledPickupDatetime = $scope.SPTTIME;
                      $scope.picreqsdata.actualPickupDatetime = $scope.SPTTIME;
                      $scope.picreqsdata.createdBy = '1';
                      $scope.picreqsdata.createdDatetime = $scope.Curre_date;
                      $scope.picreqsdata.lastUpdatedBy = '1';
                      $scope.picreqsdata.lastUpdatedDatetime = $scope.Curre_date;
                       $http({
                              url: pickupRequestDetail,
                              method: "POST",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.picreqsdata
                          
                           }).then(function(response)
                              {
                               // alert('inside data of Pickup request detail service');
                                  //console.log(response.data);
                                  $scope.picreqdetai = response.data;
                                  $scope.picreqdetailid = $scope.picreqdetai.data.id;
                                  $scope.deliverydetail($scope.picreqdetailid);
                
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                     
                 };
                 
                                  /*Passing data to the Deliver details*/
                 $scope.deliverydetail = function(Pic__ID)
                 {
                     //alert(Pic__ID);
                    // alert($scope.N_GOiD);
                  //  alert('NGO ID:   '+$scope.person_IID);
                     $scope.SP_Time = new Date($scope.DONSPT);
                    // alert($scope.SP_Time);
                     $scope.now=new Date();
                     $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                     $scope.deliver_Json = {};
                     $scope.deliver_Json.id = '1';
                     $scope.deliver_Json.pickupRequestDetail = Pic__ID;
                     $scope.deliver_Json.deliveryDatetime = $scope.SP_Time;
                     $scope.deliver_Json.deliveryReceiverId = $scope.person_IID;
                     $scope.deliver_Json.deliveryStatusId = 3;
                     $scope.deliver_Json.createdBy = '2';
                     $scope.deliver_Json.createdDatetime = $scope.Curre_date;
                     $scope.deliver_Json.lastUpdatedBy = '2';
                     $scope.deliver_Json.lastUpdatedDatetime = $scope.Curre_date;
                     
                      $http({
                              url: deliveryDetail,
                              method: "POST",
                              dataType: "json",
                              
                              headers: {
                              'Content-Type' :'application/json'
                             },
                              data:$scope.deliver_Json
                          
                           }).then(function(response)
                              {
                                // alert('inside data of Delivery  service details...');
                                // //console.log(response.data);
                                 $scope.Deliver_DataId = response.data;
                                  $scope.PAR_Ngo = false;
                                  $scope.DonorData = true;
                                $scope.ngoreqiestsindonor();
                                 
                              },
                                function(response) {
                                  alert('Errorrrrrrrrrrrrrr');

                                });
                 };
}]);
