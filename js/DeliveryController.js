
app.controller('DeliveryController',['$scope','$http','$location','$localStorage',function($scope,$http,$location,$localStorage) {

  //This will hide the DIV by default.
  $scope.IsVisible = false;

  $scope.ShowHide = function (value) {

    //If DIV is visible it will be hidden and vice versa.
    $scope.IsVisible = value == "y";
  };


  $scope.$on('gmPlacesAutocomplete::placeChanged', function(){
    var location = $scope.autocomplete.getPlace().geometry.location;

    $scope.$apply();
  });
  
  //code for showing pickup when user selects pickup option
   $scope.PI__ckup = false;
//
  
            /*Get Requests */
                    $scope.Listof_requests = function()
                    {
                      $http({
                        url: 'http://localhost:8080/api/requests',
                        method: "GET",
                        dataType: "json",

                        headers: {
                          'Content-Type' :'application/json'
                         },
                         }).then(function(response)
                          {

                            //console.log(response);
                            $scope.ReQuests_data = response.data;
                            console.log($scope.Organisation);

                             $scope.Req_Name = []; 
                             $scope.Req_Id = []; 


                             for (var i=0;i<$scope.ReQuests_data.data.length;i++)
                             {
                                     $scope.Req_Name.push($scope.ReQuests_data.data[i].purpose);
                                     $scope.Req_Id.push($scope.ReQuests_data.data[i].id);
           
                             }
                             console.log($scope.Req_Name);
                             console.log($scope.Req_Id);
                             //console.log($scope.Stae_id);

                          },
                          function(response) {
                          alert('Errorrrrrrrrrrrrrr');

                          }); 
                      };
                      
                      $scope.asds = function()
                      {
                           $scope.person_Req_Id_ID = $scope.Req_Id[$scope.Req_Name.indexOf($scope.reqInfo)];
                      }
                        
                        $scope.getorgid= function(){
                            $scope.orggid =$scope.org_id[$scope.Name_org.indexOf($scope.orgName)];
                        }
                        
                        
                        /*Pickup based data*/
                         $scope.pickup_Not = true;
                        $scope.PIC_kup = function ()
                        
                            {
                                //alert('check pickup');
                              if($scope.pickup == true)
                              {
                                $scope.PI__ckup = true;
                                 $scope.pickup_Not = false;
                              }
                              else
                              {
                                $scope.PI__ckup = false;
                                $scope.pickup_Not = true;
                              }
                            };
                            /*With out Pickup send data to database*/
                            $scope.sendWithoutPikup = function()
                            {
                                 $scope.now=new Date();
                                 $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString(); 
                                $scope.person_Req_Id_ID = $scope.Req_Id[$scope.Req_Name.indexOf($scope.reqInfo)];
                                $scope.delvDate= new Date();
                                console.log($scope.delvDate);
                                $scope.respDate= new Date();
                                console.log($scope.respDate);
                                $scope.pickup='N';
                                $http({
                                    url: 'http://localhost:8080/api/responses',
                                    method: "POST",
                                    dataType: "json",

                                    headers: {
                                    'Content-Type' :'application/json'
                                   },
                                    data :
                                    {
                                    'id':'3',
                                    'requestId':$scope.person_Req_Id_ID,
                                    'responseDate': $scope.respDate,
                                    'quantity':$scope.qty,
                                    'isDonorDelivering':$scope.pickup,
                                    'deliveryStatusId':$scope.delvMode,
                                    'deliveryDate':$scope.delvDate,
                                    "createdBy": '2',
                                    "createdDatetime": $scope.Curre_date,
                                    "lastUpdatedBy": '1',
                                    "lastUpdatedDatetime": $scope.Curre_date
                                    }
                                    }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    $scope.Response_Id = response.data;
//                                    $scope.respid=[];
//                                    for (var i=0;i<$scope.Response_Id.data.length;i++){
//                                        $scope.respid.push($scope.Response_Id.data[i].id);
//                                    }
                                    $localStorage.Res_Id = $scope.Response_Id.data.id;
                                   
                                    console.log($scope.mode);
                                    
                                    if($scope.mode == 'Organization'){
                                        //alert('case 1');
                                    $scope.orgResponse();
                                }
                                else if($scope.mode == 'Individual'){
                                    //alert('case 2');
                                    $scope.PersonResponse();
                                }
                                    //alert($scope.Res_Id);
                                    /*  $location.path('/home');*/
                                    
                                    },
                                    function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                    });

                            };
                            
                            /*orgResponse*/
                            
                             $scope.orgResponse= function(){
                                 //alert ('in org Response');
                                 $scope.respon_id=$localStorage.Res_Id;
                                 $scope.orggid =$scope.org_id[$scope.Name_org.indexOf($scope.orgName)];
                                 console.log($scope.respon_id);
                                 $scope.now=new Date();
                                 $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString(); 
                                 $http({
                                    url: 'http://localhost:8080/api/orgResponses',
                                    method: "POST",
                                    dataType: "json",

                                    headers: {
                                    'Content-Type' :'application/json'
                                   },
                                    data :
                                    {
                                    'id':'3',
                                    'responseId':$scope.respon_id,
                                    'orgId': $scope.orggid,
                                    "createdBy": '2',
                                    "createdDatetime": $scope.Curre_date,
                                    "lastUpdatedBy": '1',
                                    "lastUpdatedDatetime": $scope.Curre_date
                                    }
                                    }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    $location.path('/home');
                                    },
                                    function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                    });
                                 
                                 
                             }
                             
                             
                             /*Person Response*/
                             
                             
                             $scope.PersonResponse= function(){
                                 //alert('in Person response');
                                 
                                  $scope.respons_id=$localStorage.Res_Id;
                                  console.log($scope.respons_id);
                                  $scope.now=new Date();
                                  $scope.prsnId = $scope.persID[$scope.Ngo_NAme.indexOf($scope.PersonName)];
                                 $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString(); 
                                 $http({
                                    url: 'http://localhost:8080/api/personResponses',
                                    method: "POST",
                                    dataType: "json",

                                    headers: {
                                    'Content-Type' :'application/json'
                                   },
                                    data :
                                    {
                                    'id':'3',
                                    'responseId':$scope.respons_id,
                                    'personId': $scope.prsnId,
                                    "createdBy": '2',
                                    "createdDatetime": $scope.Curre_date,
                                    "lastUpdatedBy": '1',
                                    "lastUpdatedDatetime": $scope.Curre_date
                                    }
                                    }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    $location.path('/home');
                                    },
                                    function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                    });
                                 
                                 
                                 
                                 
                             }
                             
                             
                             
                            $scope.sendWi_Pikup = function()
                            {
                                 $scope.now=new Date();
                                 $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString(); 
                                $scope.person_Req_Id_ID = $scope.Req_Id[$scope.Req_Name.indexOf($scope.reqInfo)];
                                $scope.delvDate= new Date();
                                console.log($scope.delvDate);
                                $scope.respDate= new Date();
                                console.log($scope.respDate);
                                $scope.pickup='N';
                                $http({
                                    url: 'http://localhost:8080/api/responses',
                                    method: "POST",
                                    dataType: "json",

                                    headers: {
                                    'Content-Type' :'application/json'
                                   },
                                    data :
                                    {
                                    'id':'3',
                                    'requestId':$scope.person_Req_Id_ID,
                                    'responseDate': $scope.respDate,
                                    'quantity':$scope.qty,
                                    'isDonorDelivering':$scope.pickup,
                                    'deliveryStatusId':$scope.delvMode,
                                    'deliveryDate':$scope.delvDate,
                                    "createdBy": '2',
                                    "createdDatetime": $scope.Curre_date,
                                    "lastUpdatedBy": '1',
                                    "lastUpdatedDatetime": $scope.Curre_date
                                    }
                                    }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    $scope.Response_Id = response.data;
//                                    $scope.respid=[];
//                                    for (var i=0;i<$scope.Response_Id.data.length;i++){
//                                        $scope.respid.push($scope.Response_Id.data[i].id);
//                                    }
                                    $scope.Res_Id = $scope.Response_Id.data.id;
                                   // alert($scope.Res_Id);
                                    $scope.sendWithPickup($scope.Res_Id);
                                    /*  $location.path('/home');*/
                                    
                                    },
                                    function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                    });

                            };
                            /*With Pickup */
                            
                                $scope.sendWithPickup = function(I_D)
                                {
                                    $scope.Res_Id = I_D;
                                    
                                   // alert('value true:  '+$scope.Res_Id);
                                    $scope.pickup='Y';
                                    $scope.PI__ckup = true;
                                    $scope.schpickTime=new Date();
                                    console.log($scope.schpickTime);

                                    $scope.AcPickYime = new Date();
                                    console.log($scope.AcPickYime);

                                    $scope.now=new Date();

                                    $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                                    $scope.pick_person__ID = $scope.pickpersonID[$scope.Volunteer_NAme.indexOf($scope.pickPerson)];
                                   // alert($scope.pick_person__ID);
                                    //alert($scope.Curre_date);

//                                    $scope.pickup_data = {};
//                                    $scope.pickup_data.id = '1';
//                                    $scope.pickup_data.responseId = 

                                    $http({
                                    url: 'http://localhost:8080/api/pickup',
                                    method: "POST",
                                    dataType: "json",

                                    headers: {
                                    'Content-Type' :'application/json'
                                   },
                                    data :
                                    {
                                    "id":'2',
                                    "responseId":$scope.Res_Id,
                                    "pickupPersonId":$scope.pick_person__ID,
                                    "scheduledPickupDateTime":$scope.schpickTime,
                                    "actualPickupDateTime":$scope.AcPickYime,
                                    "deliveryStatusId":$scope.delvMode,
                                    "createdBy": '1',
                                    "createdDatetime":$scope.Curre_date,
                                    "lastUpdatedBy": '2',
                                    "lastUpdatedDatetime":$scope.Curre_date
                                    }
                                    }).then(function(response) {
                                   // alert('inside Pickup service');
                                    console.log(response);
                                    $scope.resp=response.data;
                                    console.log($scope.resp);
                                    $location.path('/home');
                                    /*  $location.path('/home');*/
                                    },
                                    function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                    });
                                };
                                
                                
                            $scope.gotoPickup=function(){
                            
                                $scope.respDate = new Date();
                                

                            if ($scope.pickup   == true ){
                                //alert('value true');
                              //  alert('value true');
                                $scope.pickup='Y';
                                 $scope.PI__ckup = true;
                                $scope.schpickTime=new Date();
                                console.log($scope.schpickTime);

                                $scope.AcPickYime = new Date();
                                console.log($scope.AcPickYime);

                              $scope.now=new Date();

                                  $scope.Curre_date = new Date($scope.now.getTime() - $scope.now.getTimezoneOffset() * 60000).toISOString();  
                                   $scope.pick_person__ID = $scope.pickpersonID[$scope.Volunteer_NAme.indexOf($scope.pickPerson)];
                                                              // alert($scope.pick_person__ID);
                                    //alert($scope.Curre_date);
                                $http({
                                  url: 'http://localhost:8080/api/pickup',
                                  method: "POST",
                                  dataType: "json",

                                  headers: {
                                    'Content-Type' :'application/json'
                                   },
                                  data :
                                    {
                                      "id":2,
                                    "responseId":"3",
                                    "pickupPersonId":$scope.pick_person__ID,
                                    "scheduledPickupDateTime":$scope.schpickTime,
                                    "actualPickupDateTime":$scope.AcPickYime,
                                    "deliveryStatusId":$scope.delvMode,
                                     "createdBy": '1',
                                        "createdDatetime":$scope.Curre_date,
                                        "lastUpdatedBy": '2',
                                        "lastUpdatedDatetime":$scope.Curre_date
                                    }
                                }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    /*  $location.path('/home');*/
                                  },
                                  function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                  });

                              }
                              else if($scope.pickup  == false){

                                   $scope.person_Req_Id_ID = $scope.Req_Id[$scope.Req_Name.indexOf($scope.reqInfo)];
                                $scope.delvDate= new Date();
                                console.log($scope.delvDate);
                                $scope.respDate= new Date();
                                 console.log($scope.respDate);
                                $scope.pickup='N';
                                 $http({
                                  url: 'http://localhost:8080/api/responses',
                                  method: "POST",
                                  dataType: "json",

                                  headers: {
                                    'Content-Type' :'application/json'
                                   },
                                  data :
                                    {
                                        'id':'3',
                                        'requestId':$scope.person_Req_Id_ID,
                                        'responseDate': $scope.respDate,
                                        'quantity':$scope.qty,
                                        'isDonorDelivering':$scope.pickup,
                                        'deliveryStatusId':$scope.delvMode,
                                        'deliveryDate':$scope.delvDate,
                                         "createdBy": '2',
                                      "createdDatetime": $scope.Curre_date,
                                      "lastUpdatedBy": '1',
                                      "lastUpdatedDatetime": $scope.Curre_date
                                    }
                            }).then(function(response) {
                                    //alert(response);
                                    console.log(response);
                                    $location.path('/home');
                                    /*  $location.path('/home');*/
                                  },
                                  function(response) {
                                    alert('Errorrrrrrrrrrrrrr');

                                  });

                               }
                            };

/*display Divs Based on user selection*/
            $scope.Organisation = false;
            $scope.person = false;
        $scope.show_orgadiv = function()
        {
            //alert('org');
            $scope.Organisation = true;
            $scope.person = false;
            
        }
         $scope.show_persondiv = function()
        {
           // alert('person');
            $scope.Organisation = false;
            $scope.person = true;
        }
        
        /*Getting Organisation name*/
        $scope.Listof_Organisation = function()
                    {
                      $http({
                        url: 'http://localhost:8080/api/organization',
                        method: "GET",
                        dataType: "json",

                        headers: {
                          'Content-Type' :'application/json'
                         },
                         }).then(function(response)
                          {

                            //console.log(response);
                            $scope.Organisation = response.data;
                            console.log($scope.Organisation);

                             $scope.Name_org = []; 
                             $scope.org_id=[];
                  //          $scope.Stae_id = []; 
                  //           $scope.Cites_id = []; 
                             //console.log($scope.Cites_id);

                             for (var i=0;i<$scope.Organisation.data.length;i++)
                             {
                                   //$scope.ALL_staes = $scope.Citi_es.data[i];
                                     $scope.Name_org.push($scope.Organisation.data[i].orgName);
                                     $scope.org_id.push($scope.Organisation.data[i].id);
                  //                   $scope.Stae_id.push($scope.Citi_es.data[i].stateId);
                  //                   $scope.Cites_id.push($scope.Citi_es.data[i].id);
                                      //$scope.ORG_type_ID.push($scope.getNGO.data[i].id);
                                      //$scope.ORG_type_Code.push($scope.getNGO.data[i].orgTypeCode);
                             }
                             console.log($scope.Name_org);
                             console.log($scope.org_id);
                             //console.log($scope.Stae_id);

                          },
                          function(response) {
                          alert('Errorrrrrrrrrrrrrr');

                          });   


                     /* $scope.startsWith = function(state, viewValue)
                      {
                        return state.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
                      };*/

                    };
                    
                    /*Pickup*/
                     $scope.get_person__role = function()
                    {
                         $http({
                        url: 'http://localhost:8080/api/person',
                        method: "GET",
                        dataType: "json",

                        headers: {
                          'Content-Type' :'application/json'
                         },
                         }).then(function(response)
                          {

                            //console.log(response);
                            $scope.Person_data = response.data;
                            
                           // $scope.person_type = $scope.Person_data.data.personType;
                            console.log($scope.Person_data);

                             $scope.person_Type = []; 
                             $scope.Volunteer_NAme = [];
                              $scope.Ngo_NAme= [];
                              $scope.pickpersonID=[];
                              $scope.persID=[];
                  //          $scope.Stae_id = []; 
                  //           $scope.Cites_id = []; 
                             //console.log($scope.Cites_id);

                             for (var i=0;i<$scope.Person_data.data.length;i++)
                             {
                                   //$scope.ALL_staes = $scope.Citi_es.data[i];
                                     $scope.person_Type.push($scope.Person_data.data[i].personTypeId);
                                     $scope.pers_type =  $scope.person_Type[i];
                                     if($scope.pers_type == '3')
                                     {
                                         $scope.Volunteer_NAme.push($scope.Person_data.data[i].fullName);
                                         $scope.pickpersonID.push($scope.Person_data.data[i].id);
                                     }
                                     if($scope.pers_type == '1')
                                     {
                                   $scope.Ngo_NAme.push($scope.Person_data.data[i].fullName);
                                    $scope.persID.push($scope.Person_data.data[i].id);

                                     }
//                                     if(i == 3)
//                                      break;
                               }
                             console.log($scope.person_Type);
                             console.log($scope.Volunteer_NAme);
                             console.log($scope.Ngo_NAme);
                             console.log($scope.pickpersonID);
                             console.log($scope.persID);
                             
                             //console.log($scope.Stae_id);

                          },
                          function(response) {
                          alert('Errorrrrrrrrrrrrrr');

                          });   
                          

                    };
                     $scope.get_pickperson_ID = function()
                            {
                                 $scope.pick_person__ID = $scope.pickpersonID[$scope.Volunteer_NAme.indexOf($scope.pickPerson)];
                            };

                                $scope.getPrsnID= function(){
                                    $scope.prsnId = $scope.$scope.persID[$scope.Ngo_NAme.indexOf($scope.PersonName)];
                                    
                                };
}]);


angular.module('OtdDirectives', []).
directive('googlePlaces', function(){
  return {

    restrict:'E',
    replace:true,
    // transclude:true,
    scope: {location:'='},
    template: '<input id="google_places_ac" name="google_places_ac" type="text" class="form-control"/>',
    link: function($scope, elm, attrs){

      var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], {});
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        $scope.location = place.geometry.location.lat()+','+ place.geometry.location.lng() ;

        $scope.$apply();


      });
    }
  };
});


