/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 function getUrl(service) {
    return "http://" + host + ":" + port + service;
}

var app = angular.module("app", ['angularGAPI','ngRoute','ngStorage','angularUtils.directives.dirPagination']);

	app.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'views/home.html',
				
			})
                       
                        
                         .when('/pickup', {
				templateUrl : 'views/pickup.html',
				
			})
                          .when('/CurrentRequests', {
				templateUrl : 'views/CurrentRequests.html',
				
			})
                        
                         .when('/profile', {
				templateUrl : 'views/profile.html',
				
			})

			// route for the about page
			.when('/Ngo_reg', {
				templateUrl : 'views/Ngo_reg.html',
				
			})

			// route for the contact page
			.when('/Donor_Volunteer', {
				templateUrl : 'views/Donor_Volunteer.html',
				
			})
                        .when('/aboutus', {
				templateUrl : 'views/aboutus.html',
				
			})                     
                        
                        
                         .when('/showrequest', {
				templateUrl : 'views/showrequest.html',
				
			})
                        
                        .when('/contactus', {
				templateUrl : 'views/contactus.html',
				
			})
                         .when('/FAQS', {
				templateUrl : 'views/FAQS.html',
				
			})
                          .when('/gallery', {
				templateUrl : 'views/gallery.html',
				
			})
                         .when('/privacypolicy', {
				templateUrl : 'views/privacypolicy.html',
				
			})
                         .otherwise({
                            redirectTo: '/'
                          });
	});
    app.controller("HelloCtrl", function() {
 


    });

  window.fbAsyncInit = function() {
              FB.init({

                appId: '1599594036718634',
                secret:'8cc0cba8ef12f248848c9af9386e5e61',

                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                oauth: true, // enable OAuth 2.0
                xfbml: true, // parse XFBML// App ID
                version    : 'v2.10'
              });
            };
            (function(d, s, id){
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) {return;}
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            
            //Google API
            
            function onLoadFunction()
            {  
                gapi.client.setApiKey('AIzaSyATLJIh53nk-HpLK7BrlzUzMBvtOK4XXv8');
                gapi.client.load('plus','v1',function(){

                });
            };
            
			angular.module('app').run(function($http)
			{
				 $http.defaults.headers.common.Authorization = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhYmNkIiwicm9sZSI6IlVTRVIiLCJpZCI6IjE5IiwiaWF0IjoxNDU3ODQwMjQ0fQ.8WbHJhy4Ae43Yuh0Zq8na92GZ14FceBiMx5Q76RE79A';
				 
			});